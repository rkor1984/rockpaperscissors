using System.Data.SqlClient;

namespace RockPaperScissors.IDbInteractions
{
    public interface IConnectionFactory
    {
        SqlConnection Connection { get; }
        SqlConnection NewSession();
    }
}