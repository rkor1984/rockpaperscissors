﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Newtonsoft.Json;
using RockPaperScissors.DbInteractions;
using RockPaperScissors.Entities.IEntities;

namespace RockPaperScissors
{
    public static class DbContextExtension
    {

        public static bool AllMigrationsApplied(this DbContext context)
        {
            var applied = context.GetService<IHistoryRepository>()
                .GetAppliedMigrations()
                .Select(m => m.MigrationId);

            var total = context.GetService<IMigrationsAssembly>()
                .Migrations
                .Select(m => m.Key);

            return !total.Except(applied).Any();
        }

        public static void EnsureSeeded(this GameContext context)
        {
            if (context.Players.Any()) return;
            var types = JsonConvert.DeserializeObject<List<IPlayer>>(File.ReadAllText("seed" + Path.DirectorySeparatorChar + "Players.json"));
            context.AddRange(types);
            context.SaveChanges();
        }
    }

}
