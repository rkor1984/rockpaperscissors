﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using Remotion.Linq.Parsing;
using RockPaperScissors.DbInteractions;
using RockPaperScissors.Entities;
using RockPaperScissors.Entities.Enums;

namespace RockPaperScissors
{
    public interface IGameRules
    {
        IEnumerable<Player> PlayGame(Choice humanChoice, int gameId);
    }

    public class GameRules : IGameRules
    {
        private readonly IDistributedCache _cache;
        private readonly GameContext _context;


        public GameRules(IDistributedCache cache, GameContext context)
        {
            _cache = cache;
            _context = context;
        }
      

        public IEnumerable<Player> PlayGame(Choice humanChoice, int gameId)
        {

            var game = _context.Games.FirstOrDefault(x => x.GameId == gameId);
            game.NumberOfTries--;
            var player1 = _context.Players.FirstOrDefault(x => x.PlayerId == game.Player1);
            var player2 = _context.Players.FirstOrDefault(x => x.PlayerId == game.Player2);
            UpdateChoice(player1, humanChoice);
            UpdateChoice(player2, humanChoice);
            UpdateScore(player1, player2);
            _context.SaveChanges();
            return _context.Players.Where(x => x.PlayerId == game.Player1 || x.PlayerId == game.Player2);
        }

        private void UpdateScore(Player playerA, Player playerB)
        {
            if (Player1Wins(playerA, playerB))
            {
                playerA.Score++;
            }
            else if (Player1Wins(playerB, playerA))
            {
                playerB.Score++;
            }
        }

        private static bool Player1Wins(Player player1, Player player2)
        {
            return player1.PlayerChoice == Choice.Scissors && player2.PlayerChoice == Choice.Paper
                   || player1.PlayerChoice == Choice.Paper && player2.PlayerChoice == Choice.Rock
                   || player1.PlayerChoice == Choice.Rock && player2.PlayerChoice == Choice.Scissors;
        }

        private void UpdateChoice(Player player, Choice choice)
        {
            var playerType = player.PlayerType;
            switch (playerType)
            {
                case PlayerTypeEnum.TacticComputer:
                    TacticComputerChoice(player);
                    break;
                case PlayerTypeEnum.RandomComputer:
                    RandomComputerChoice(player);
                    break;
                case PlayerTypeEnum.Human:
                    HumanChoice(player, choice);
                    break;
                default:
                    throw new InvalidPlayerType();

            }
        }

        private void HumanChoice(Player player, Choice choice)
        {
            player.PlayerChoice = choice;
        }

        private void TacticComputerChoice(Player player)
        {
            try
            {
                var lastTacticChoice = _cache.Get("lastTacticChoice");
                if (lastTacticChoice == null)
                {
                    const Choice initialChoice = Choice.Paper;
                    CacheCurrentChoice(initialChoice);
                    player.PlayerChoice = initialChoice;
                    return;
                }
                var lastChoice = Encoding.UTF8.GetString(lastTacticChoice);
                var enumChoice = int.Parse(lastChoice);
                var choseTactically = ChoseTactically((Choice)enumChoice);
                CacheCurrentChoice(choseTactically);

                player.PlayerChoice = choseTactically;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        private void CacheCurrentChoice(Choice choseTactically)
        {
            _cache.Set("lastTacticChoice"
                , Encoding.UTF8.GetBytes(((int)choseTactically).ToString()));
        }

        private static Choice ChoseTactically(Choice enumChoice)
        {
            switch (enumChoice)
            {
                case Choice.Paper:
                    return Choice.Scissors;
                case Choice.Scissors:
                    return Choice.Rock;
                case Choice.Rock:
                    return Choice.Paper;
                default:
                    return Choice.Paper;
            }
        }

        private void RandomComputerChoice(Player player)
        {
            var values = Enum.GetValues(typeof(Choice));
            var random = new Random();
            var randomChoice = (Choice)values.GetValue(random.Next(values.Length));

            player.PlayerChoice = randomChoice;
        }
    }
}