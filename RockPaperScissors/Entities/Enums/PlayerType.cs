using System.ComponentModel.DataAnnotations;

namespace RockPaperScissors.Entities.Enums
{
    public enum PlayerTypeEnum
    {
        [Display(Name = "Human")]
        Human,
        [Display(Name = "Random Computer")]
        RandomComputer,
        [Display(Name = "Tactic Computer")]
        TacticComputer
    }
}