namespace RockPaperScissors.Entities.Enums
{
    public enum Choice
    {
        Rock,
        Paper,
        Scissors
    }
}