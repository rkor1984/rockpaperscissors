﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace RockPaperScissors.DbInteractions
{
    public class GameContextFactory : IDesignTimeDbContextFactory<GameContext>
    {
        public GameContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<GameContext>();
            optionsBuilder.UseInMemoryDatabase("myDatabase")
                //.UseSqlServer("sqlserver conection string")
                ;
            return new GameContext(optionsBuilder.Options);
        }
    }
}