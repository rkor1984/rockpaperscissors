﻿CREATE TABLE [dbo].[PlayerType]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [PlayerTypeName] NVARCHAR(100) NOT NULL
)
