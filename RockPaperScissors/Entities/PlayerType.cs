﻿using System.ComponentModel.DataAnnotations;

namespace RockPaperScissors.Entities
{
    class PlayerType
    {
        [Key]
        public int Id { get; set; }

        public string PlayerTypeName { get; set; }
    }
}
