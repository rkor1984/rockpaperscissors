﻿CREATE TABLE [dbo].[Game]
(
	[GameId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [GameCreatedDate] DATETIME NOT NULL, 
    [Player1] NVARCHAR(100) NOT NULL, 
    [Player2] NVARCHAR(100) NOT NULL, 
    [NumberOfTries] INT NOT NULL DEFAULT 0
)
