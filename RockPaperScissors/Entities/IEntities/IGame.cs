﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace RockPaperScissors.Entities.IEntities
{
    public interface IGame
    {
        [DisplayName("Game Created Date")]
        DateTime GameCreatedDate { get; set; }

        [Key]
        int GameId { get; set; }

        [DisplayName("Player 1")]
        int Player1 { get; set; }

        [DisplayName("Player 2")]
        int Player2 { get; set; }

        [DisplayName("Number Of Tries")]
        int NumberOfTries { get; set; } 
    }
}