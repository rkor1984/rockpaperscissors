using System.Collections.Generic;
using RockPaperScissors.Entities.IEntities;

namespace RockPaperScissors.DbInteractions.IDbInteractions
{
    public interface IDbInteractions
    {
        IEnumerable<IPlayer> GetPlayers();
    }
}