﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using RockPaperScissors;
using RockPaperScissors.Communications;
using RockPaperScissors.DbInteractions;
using RockPaperScissors.Entities;
using RockPaperScissors.Entities.Enums;
using RockPaperScissorsApp.Models;
using SQLitePCL;

namespace RockPaperScissorsApp.Controllers
{

    public class GameController : Controller
    {
        private readonly GameRules _rules;
        private readonly GameContext _context;

        private readonly IMediator _mediator;

        public GameController(GameRules rules,
            GameContext _context,
            IMediator mediator)
        {
            _rules = rules;
            this._context = _context;
            _mediator = mediator;
        }

        // GET: CreateGame
        [HttpGet]
        public IActionResult StartGame()
        {
            ViewBag.Players = new SelectList(_context.Players.ToList(), "PlayerId", "PlayerName");
            return View();
        }

        // Post: CreateGame
        [HttpPost]
        public async Task<IActionResult> StartGame(GameRequest gameRequest, Game game)
        {
            var response = await _mediator.Send(gameRequest);
            return RedirectToAction("PlayGame", new { id = response.GameId });
        }

        // GET: Play
        [HttpGet("PlayGame")]
        public IActionResult PlayGame(int id)
        {
            var gameViewModel = GameDetails(id);

            return View(gameViewModel);
        }

        private GameViewModel GameDetails(int id)
        {
            var gameViewModel = new GameViewModel
            {
                Game = _context.Games.FirstOrDefault(x => x.GameId == id)
            };
            gameViewModel.Players = _context.Players.Where(x => gameViewModel.Game.Player1 == x.PlayerId
            || gameViewModel.Game.Player2 == x.PlayerId).ToList();
            return gameViewModel;
        }

        [HttpPost]
        public IActionResult Play([FromForm]Choice humanChoice, [FromForm]GameViewModel gameModel)
        {
            var choices = _rules.PlayGame(humanChoice, gameModel.Game.GameId);
            return RedirectToAction("PlayGame", new { id = gameModel.Game.GameId });
        }

        // GET: Game/Details/5
        public ActionResult PlayScreen()
        {
            return View();
        }

        // GET: Game/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Game/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Game/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Game/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Game/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Game/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}