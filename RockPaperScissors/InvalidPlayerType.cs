﻿using System;
using System.Runtime.Serialization;

namespace RockPaperScissors
{
    [Serializable]
    internal class InvalidPlayerType : Exception
    {
        public InvalidPlayerType()
        {
        }

        public InvalidPlayerType(string message) : base(message)
        {
        }

        public InvalidPlayerType(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidPlayerType(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}