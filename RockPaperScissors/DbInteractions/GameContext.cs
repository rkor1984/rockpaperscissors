﻿using Microsoft.EntityFrameworkCore;
using RockPaperScissors.Entities;

namespace RockPaperScissors.DbInteractions
{
    public class GameContext : DbContext
    {
        public GameContext(DbContextOptions<GameContext> options)
            : base(options)
        {
          
        }
        public GameContext()
        {

        }

        public virtual DbSet<Player> Players { get; set; }

        public virtual DbSet<Game> Games { get; set; }
       
    }
}