﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;
using RockPaperScissors.Entities.Enums;

namespace RockPaperScissors.Entities.IEntities
{
    public interface IPlayer
    {
        [Key]
        int PlayerId { get; }

        [DisplayName("Player Type")]
        PlayerTypeEnum PlayerType { get; set; }

        [DisplayName("Player Name")]
        string PlayerName { get; set; }

        [DisplayName("Player Created Date")]
        DateTime PlayerCreatedDate { get; set; }

        [DisplayName("Player Choice")]
        Choice? PlayerChoice { get; set; }

        [DisplayName("Score")]
        int Score { get; set; }
    }
}
