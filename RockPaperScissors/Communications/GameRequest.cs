﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RockPaperScissors.DbInteractions;
using RockPaperScissors.Entities;

namespace RockPaperScissors.Communications
{
    public class GameRequest : IRequest<GameResponse>
    {
        public int Player1 { get; set; }
        public int Player2 { get; set; }
    }

    public class GameHandler : IRequestHandler<GameRequest, GameResponse>
    {
        private readonly GameContext _context;

        public GameHandler(GameContext context)
        {
            _context = context;
        }

        public Task<GameResponse> Handle(GameRequest request, CancellationToken cancellationToken)
        {
            var game = new Game
            {
                GameCreatedDate = DateTime.Now,
                NumberOfTries = 3,
                Player1 = request.Player1,
                Player2 = request.Player2

            };
            _context.Games.Add(game);
            _context.Players.FirstOrDefault(x => x.PlayerId == game.Player1).Score = 0;
            _context.Players.FirstOrDefault(x => x.PlayerId == game.Player2).Score = 0;

            _context.SaveChanges();

            return Task.FromResult(new GameResponse
            {
                GameId = game.GameId
            });
        }
    }

    public class GameResponse
    {
        public int GameId { get; set; }
    }
}
