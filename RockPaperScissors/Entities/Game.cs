﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using RockPaperScissors.Entities.Enums;
using RockPaperScissors.Entities.IEntities;

namespace RockPaperScissors.Entities
{
    public class Game : IGame
    {
        [Key]
        public int GameId { get; set; }

        [DisplayName("Game Created Date")]
        public DateTime GameCreatedDate { get; set; }

        //a game consists of two players only
        [DisplayName("Player 1")]
        public int Player1 { get; set; }

        [DisplayName("Player 2")]
        public int Player2 { get; set; }

        [DisplayName("Number Of Tries")]
        public int NumberOfTries { get; set; }
    }
}