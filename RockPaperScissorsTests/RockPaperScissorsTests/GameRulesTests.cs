﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Moq;
using NSubstitute;
using NSubstitute.Core;
using RockPaperScissors;
using RockPaperScissors.DbInteractions;
using RockPaperScissors.Entities;
using RockPaperScissors.Entities.Enums;
using RockPaperScissors.Entities.IEntities;
using Shouldly;
using Xunit;

namespace RockPaperScissorsTests.RockPaperScissorsTests
{
    public class GameRulesTests
    {
        private readonly GameRules _rules;
        private readonly Mock<IDistributedCache> _cacheMock;
        private readonly Mock<GameContext> _context;

        public GameRulesTests()
        {
            _cacheMock = new Mock<IDistributedCache>();
            _context = new Mock<GameContext>();
            _rules = new GameRules(_cacheMock.Object, _context.Object);
        }

        [Theory,
            InlineData(Choice.Scissors, Choice.Paper),
            InlineData(Choice.Paper, Choice.Rock),
            InlineData(Choice.Rock, Choice.Scissors)
            ]
        public void TacticComputersCurrentChoiceShouldDefeatItsLastChoice(Choice predictedChoice, Choice lastTacticChoice)
        {
            //arrange
            FakeGameData(lastTacticChoice);

            //action
            var updatedPlayers = _rules.PlayGame(Choice.Scissors, 1).ToList();

            //assert
            var tacticChoice = updatedPlayers.FirstOrDefault(x => x.PlayerType == PlayerTypeEnum.TacticComputer)?.PlayerChoice;
            tacticChoice.ShouldBe(predictedChoice);

        }

        [Theory,
         InlineData(Choice.Scissors, Choice.Paper, Choice.Rock),
         InlineData(Choice.Paper, Choice.Rock, Choice.Scissors),
         InlineData(Choice.Rock, Choice.Scissors, Choice.Paper)
        ]
        public void HumanChoiceDefeatingTacticComputerThenHumanScoreShouldBeIncrementbyOne(Choice predictedChoice, Choice lastTacticChoice, Choice humanChoice)
        {
            //arrange
            FakeGameData(lastTacticChoice);

            //action
            var updatedPlayers = _rules.PlayGame(humanChoice, 1).ToList();

            //assert
            var TacticComputerScore = updatedPlayers.FirstOrDefault(x => x.PlayerType == PlayerTypeEnum.TacticComputer)?.Score;
            var humanScore = updatedPlayers.FirstOrDefault(x => x.PlayerType == PlayerTypeEnum.Human)?.Score;
            TacticComputerScore.ShouldBe(0);
            humanScore.ShouldBe(1);
        }

        [Theory,
         InlineData(Choice.Scissors, Choice.Paper, Choice.Paper),
         InlineData(Choice.Paper, Choice.Rock, Choice.Rock),
         InlineData(Choice.Rock, Choice.Scissors, Choice.Scissors)
        ]
        public void TacticComputerDefeatingHumanChoiceThenTacticComputerScoreShouldBeIncrementbyOne(Choice predictedChoice, Choice lastTacticChoice, Choice humanChoice)
        {
            //arrange
            FakeGameData(lastTacticChoice);

            //action
            var updatedPlayers = _rules.PlayGame(humanChoice, 1).ToList();

            //assert
            var humanScore = updatedPlayers.FirstOrDefault(x => x.PlayerType == PlayerTypeEnum.Human)?.Score;
            var TacticComputerScore = updatedPlayers.FirstOrDefault(x => x.PlayerType == PlayerTypeEnum.TacticComputer)?.Score;
            humanScore.ShouldBe(0);
            TacticComputerScore.ShouldBe(1);
        }

        [Theory,
         InlineData(Choice.Scissors, Choice.Paper, Choice.Scissors),
         InlineData(Choice.Paper, Choice.Rock, Choice.Paper),
         InlineData(Choice.Rock, Choice.Scissors, Choice.Rock)
        ]
        public void TacticComputerChoiceSameAsHumanChoiceThenScoreShouldBeEqualForBoth(Choice predictedChoice, Choice lastTacticChoice, Choice humanChoice)
        {
            //arrange
            FakeGameData(lastTacticChoice);

            //action
            var updatedPlayers = _rules.PlayGame(humanChoice, 1).ToList();

            //assert
            var humanScore = updatedPlayers.FirstOrDefault(x => x.PlayerType == PlayerTypeEnum.Human)?.Score;
            var TacticComputerScore = updatedPlayers.FirstOrDefault(x => x.PlayerType == PlayerTypeEnum.TacticComputer)?.Score;
            TacticComputerScore.ShouldBe(humanScore);
        }

        private void FakeGameData(Choice lastTacticChoice)
        {
            var game = new List<Game>
            {
                new Game
                {
                    GameCreatedDate = DateTime.Now,
                    GameId = 1,
                    Player1 = 1,
                    Player2 = 2
                }
            }.AsQueryable();
            var gameSet = new Mock<DbSet<Game>>();
            gameSet.As<IQueryable<Game>>().Setup(m => m.Provider).Returns(game.Provider);
            gameSet.As<IQueryable<Game>>().Setup(m => m.Expression).Returns(game.Expression);
            gameSet.As<IQueryable<Game>>().Setup(m => m.ElementType).Returns(game.ElementType);
            gameSet.As<IQueryable<Game>>().Setup(m => m.GetEnumerator()).Returns(game.GetEnumerator());
            _context.Setup(m => m.Games).Returns(gameSet.Object);
            _context.Setup(c => c.Games).Returns(gameSet.Object);

            var players = new List<Player>
            {
                new Player
                {
                    PlayerType = PlayerTypeEnum.Human,
                    PlayerCreatedDate = DateTime.Now,
                    PlayerId = 1,
                    PlayerName = "rupi",
                    Score = 0
                },
                new Player
                {
                    PlayerType = PlayerTypeEnum.TacticComputer,
                    PlayerCreatedDate = DateTime.Now,
                    PlayerId = 2,
                    PlayerName = "Tactic Computer",
                    Score = 0
                }
            }.AsQueryable();
            var palyerSet = new Mock<DbSet<Player>>();
            palyerSet.As<IQueryable<Player>>().Setup(m => m.Provider).Returns(players.Provider);
            palyerSet.As<IQueryable<Player>>().Setup(m => m.Expression).Returns(players.Expression);
            palyerSet.As<IQueryable<Player>>().Setup(m => m.ElementType).Returns(players.ElementType);
            palyerSet.As<IQueryable<Player>>().Setup(m => m.GetEnumerator()).Returns(players.GetEnumerator());

            _context.Setup(m => m.Players).Returns(palyerSet.Object);
            _context.Setup(c => c.Players).Returns(palyerSet.Object);

            _cacheMock
                .Setup(s => s.Get("lastTacticChoice"))
                .Returns(Encoding.ASCII.GetBytes(((int)lastTacticChoice).ToString()));
        }
    }
}
