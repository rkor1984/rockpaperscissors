﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using RockPaperScissors;
using RockPaperScissors.DbInteractions;
using RockPaperScissorsApp.Controllers;

namespace RockPaperScissorsApp
{
    public static class ServiceExtensions
    {
        //changes
        public static IServiceCollection RegisterServices(
            this IServiceCollection services)
        {
            services.AddTransient<GameRules>();
            return services;
        }
    }
}
