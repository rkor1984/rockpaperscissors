﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using RockPaperScissors.DbInteractions;
using RockPaperScissors.Entities;
using RockPaperScissors.Entities.IEntities;

namespace RockPaperScissorsWebApi.Controllers
{
    public class PlayersController : Controller
    {
        private readonly GameContext _context;

        public PlayersController(GameContext context)
        {
            _context = context;
        }

        // GET api/values
        [HttpGet]
        public IActionResult Index()
        {
            var players = _context.Players.ToList();
            return View(players);
        }

        // GET api/values/5
        [HttpGet("Players/Player/{id}")]
        public IActionResult Get(int id)
        {
            var player = _context.Players.Find(id);
            if (player == null)
                return NotFound();
            return View(player);
        }

        // Get 
        [HttpGet("Players/Create")]
        public IActionResult CreateNewPlayer()
        {
            return View(new Player());
        }
        // POST api/values
        [HttpPost("Players/Create")]
        public IActionResult CreateNewPlayer(Player value)
        {
            value.PlayerCreatedDate=DateTime.Now;
            _context.Players.Add(value);
            _context.SaveChanges();

            return RedirectToAction("Player", new { id = value.PlayerId });
        }

        // PUT api/values/5
        [HttpPut("Players/edit/{id}")]
        public IActionResult Update(int id, [FromBody]Player value)
        {
            var player = _context.Players.Find(id);
            if (player == null)
            {
                return NotFound();
            }

            player.PlayerType = value.PlayerType;
            player.PlayerChoice = value.PlayerChoice;

            _context.Players.Update(player);
            _context.SaveChanges();
            return RedirectToAction("Player",new {id=value.PlayerId});
        }

        [HttpGet("Players/Delete/{id}")]
        public IActionResult Delete(int id)
        {
            var player = _context.Players.Find(id);
            if (player == null)
            {
                return NotFound();
            }
            return View(player);
        }

        [HttpPost("Players/Delete/{id}")]
        public IActionResult DeletePlayer(int id)
        {
            var player = _context.Players.Find(id);
            if (player == null)
            {
                return NotFound();
            }

            _context.Players.Remove(player);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
