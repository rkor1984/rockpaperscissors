﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using RockPaperScissors.Entities.Enums;
using RockPaperScissors.Entities.IEntities;

namespace RockPaperScissors.Entities
{
    public class Player : IPlayer
    {
        [Key]
        [DisplayName("Player Id")]
        public int PlayerId { get; set; }

        [DisplayName("Player Type")]
        public PlayerTypeEnum PlayerType { get; set; }

        [DisplayName("Player Name")]
        public string PlayerName { get; set; }

        [DisplayName("Player Created Date")]
        public DateTime PlayerCreatedDate { get; set; }

        [DisplayName("Player Choice")]
        public Choice? PlayerChoice { get; set; }

        [DisplayName("Score")]
        public int Score { get; set; } 
    }
}
