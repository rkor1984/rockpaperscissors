﻿CREATE TABLE [dbo].[Player]
(
	[PlayerId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [PalyerType] INT NOT NULL, 
    [PlayerName] NVARCHAR(100) NOT NULL, 
    [PlayerCretaedDate] DATETIME NOT NULL, 
    [PlayerChoice] INT NULL, 
    [Score] INT NOT NULL DEFAULT 0
)
