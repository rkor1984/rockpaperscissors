﻿using System.Collections.Generic;
using System.Linq;
using RockPaperScissors.Entities;

namespace RockPaperScissorsApp.Models
{
    public class GameViewModel
    {
        public List<Player> Players { get; set; }
        public Game Game { get;  set; }
    }
}